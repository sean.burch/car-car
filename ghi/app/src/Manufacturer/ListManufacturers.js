import React, { useState, useEffect }  from "react";


function ManufacturerList() {
    const [manufacturer, setManufacturer] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok) {
            const { manufacturers } = await response.json()
            setManufacturer(manufacturers)
        } else {
            console.error('An error occured fetching the data')
        }
    }

    useEffect(() => {
        fetchData()
    }, [])
    
    return (
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturer?.map((manufacturer) => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList
