import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Nav from './Nav'
import TechnicianList from './TechnicianList'
import TechnicianForm from './TechnicianForm'
import AppointmentForm from './AppointmentForm'
import AppointmentList from './AppointmentList'
import ServiceHistoryList from './ServiceHistoryList'
import VehicleModelForm from './VehicleModelForm'
import AutomobileList from './AutomobileList'
import AutomobileForm from './AutomobileForm'
import CustomerForm from './Sales/Customer/CustomerForm';
import CustomerList from './Sales/Customer/CustomerList';
import SalesForm from './Sales/SalesRecords/SalesForm';
import SalesList from './Sales/SalesRecords/SalesList';
import SalesPersonForm from './Sales/Employee/SalesPersonForm';
import SalesPersonList from './Sales/Employee/SalesPersonList';
import SalesPersonHistory from './Sales/Employee/SalesHistoryList';
import Manufacturerlist from './Manufacturer/ListManufacturers';
import ManufacturerForm from './Manufacturer/ManufacturerForm';
import ModelsList from './ModelsList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers">
            <Route path="" element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalesPersonList />} />
            <Route path="create" element={<SalesPersonForm />} />
            <Route path="select" element={<SalesPersonHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<Manufacturerlist />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList />} />
          </Route>
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="technicians/create" element={<TechnicianForm />} />
          <Route path="appointments" element={<AppointmentList />} />
          <Route path="appointments/create" element={<AppointmentForm />} />
          <Route path="appointments/history" element={<ServiceHistoryList />} />
          <Route path="models/new" element={<VehicleModelForm />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
