import React, { useState, useEffect } from 'react'


function TechnicianForm(){
    const [employeeId, setEmployeeId] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId
        }

        const url = `http://localhost:8080/api/technicians/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }

            const response = await fetch(url, fetchConfig)
            if (response.ok) {
                const data = await response.json()
                setEmployeeId('')
                setFirstName('')
                setLastName("")
                }
            }


    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }


    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }


    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="add-technician">
                    <div className="text-center mb-3">
                        <span className="fs-3">Add Technician</span>
                    </div>
                    <div className="form-floating mb-3">
                        <input value ={employeeId} onChange={handleEmployeeIdChange} className="form-control" name="employee_id" id="employee_id" placeholder="Employee_id" />
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={firstName} onChange={handleFirstNameChange} className="form-control" name="first_name" id="first_name" placeholder="First_name" />
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={lastName} onChange={handleLastNameChange} className="form-control"  name="last_name" id="last_name" placeholder="Last_name" />
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
    }

    export default TechnicianForm
