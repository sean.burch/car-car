import React, { useEffect, useState } from 'react'

function SaleForm() {
    const [automobiles, setAutomobile] = useState([])
    const [salesPerson, setSalesPerson] = useState([])
    const [customer, setCustomer] = useState([])

    const  [formData, setFormData] = useState({
        vin: '',
        salesperson: '',
        customer: '',
        price: '',
    })

    const getAutomobile = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')

        if (response.ok) {
            const data = await response.json()
            const auto = data.autos.filter(autos => autos.sold === false)
            setAutomobile(auto)
        }
    }

    const getSalesPerson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/')

        if (response.ok) {
            const data = await response.json()
            setSalesPerson(data.salesperson)
        }
    }

    const getCustomer = async () => {
        const response = await fetch('http://localhost:8090/api/customers/')

        if (response.ok) {
            const data = await response.json()
            setCustomer(data.customer)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = `http://localhost:8090/api/sales/`

        const requestData = {
            ...formData,
            automobile: formData.vin,
            salesperson: formData.salesperson,
            customer: formData.customer,
            price: formData.price,
        }
        delete requestData.vin

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(requestData),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {

            const automobileUrl = `http://localhost:8100/api/automobiles/${formData.vin}/`
            const automobileConfig = {
                method: "PUT",
                body: JSON.stringify({ ...automobiles, sold: true}),
                headers: {
                    'Content-Type': 'application/json',
                }
            }

            const automobileResponse = await fetch(automobileUrl, automobileConfig)

            setFormData({
                vin: '',
                salesperson: '',
                customer: '',
                price: '',
            })
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    useEffect(() => {
        getAutomobile()
        getSalesPerson()
        getCustomer()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create_sale">
                        <div className="text-center mb-3">
                            <span className="fs-3">Record a new sale</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="Automobile_VIN">Automobile VIN</label>
                            <select value={formData.vin} onChange={handleFormChange} className="form-select" name="vin" id="vin">
                                <option defaultValue="">--Choose an autombile VIN--</option>
                                {automobiles.map(autos => {
                                    return (<option key={autos.id} value={autos.vin}>{autos.vin}</option>)
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="Salesperson">Salesperson</label>
                            <select value={formData.salesperson} onChange={handleFormChange} className="form-select" name="salesperson" id="salesperson">
                                <option defaultValue="">--Choose salesperson--</option>
                                {salesPerson.map(salespeople => {
                                    return (<option key={salespeople.id} value={salespeople.id}>{salespeople.first_name}</option>)
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="Customer">Customer</label>
                            <select value={formData.customer} onChange={handleFormChange} className="form-select" name="customer" id="customer">
                                <option defaultValue="">--Choose Customer--</option>
                                {customer.map(customer => {
                                    return (<option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>)
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.price} name="price" id="price" placeholder="Price" />
                            <label htmlFor="phone_number">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SaleForm


// {automobiles.filter(autos => (!automobiles.sold)).map(autos => {
//     return (<option key={autos.id} value={autos.vin}>{autos.vin}</option>)
// })}


// {automobiles.filter(autos => autos.sold === false) {
//     return (<option key={autos.id} value={autos.vin}>{autos.vin}</option>)
// })}


// {automobiles.filter(automobiles => (!automobiles.sold)).map(autos => {
//     return (<option key={autos.id} value={autos.vin}>{autos.vin}</option>)
// })}


// {automobiles.map((autos) => {
//     if (!autos.sold) {
//         return (<option key={autos.id} value={autos.vin}>{autos.vin}</option>)
//     }
// })}
