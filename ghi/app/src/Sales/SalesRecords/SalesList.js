import { useState, useEffect } from 'react';

function SalesList() {
    const [sale, setSales] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const { sale } = await response.json();
            setSales(sale);
        }else {
            console.error('An error occured fetching the data')
        }
    }

    useEffect(()=> {
        fetchData()
    }, []);

    return (
        <div>
            <h1>Sales</h1>
            <div className="row">
                <div className="offset-0 col-12">
                    <div className="shadow p-4 mt-4">
                        <div>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Salesperson Name</th>
                                        <th>Customer</th>
                                        <th>VIN</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {sale.map((sale) => {
                                        return (
                                            <tr key={sale.id}>
                                                <td>{sale.salesperson.employee_id}</td>
                                                <td>{sale.salesperson.first_name}</td>
                                                <td>{sale.customer.first_name}</td>
                                                <td>{sale.automobile.vin}</td>
                                                <td>{sale.price}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SalesList;
